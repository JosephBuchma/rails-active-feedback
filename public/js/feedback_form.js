var form = '
<form class="new_ticket" id="new_ticket" action="/tickets" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="✓">
<div class="field-grop">
  <label for="ticket_department_id">Department</label><select class="form-control" name="ticket[department_id]" id="ticket_department_id"><option value="">Please select</option>
  <option value="1">General support</option>
  <option value="2">Sales</option>
  <option value="3">Security</option></select>
</div>
<div class="field-grop">
  <label for="ticket_subject">Subject</label><input class="form-control" type="text" name="ticket[subject]" id="ticket_subject">
</div>
<div class="field-grop">
  <label for="ticket_body">Body</label><textarea class="form-control" name="ticket[body]" id="ticket_body"></textarea>
</div>
<div class="field-grop">
  <label for="ticket_Your name">Your name</label><input class="form-control" type="text" name="ticket[consumer_name]" id="ticket_consumer_name">
</div>
<div class="field-grop">
  <label for="ticket_Your email">Your email</label><input class="form-control" type="text" name="ticket[consumer_email]" id="ticket_consumer_email">
</div>
<div class="actions">
  <input type="submit" name="commit" value="Send" class="btn btn-primary">
</div>
</form>
'