class ActiveSupport::TimeWithZone
  def as_json(options = {})
    to_formatted_s(:short)
  end
end