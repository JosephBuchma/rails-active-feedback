###
# Overriding 'differ' gem default HTML formatter
# 
Differ::Format::HTML.module_eval do
  class << self
    private

    def as_insert(change)
      %Q{<ins>#{change.insert}</ins>}
    end

    def as_delete(change)
      %Q{<del>#{change.delete}</del>}
    end

  end
end

Differ.format = :html
