Figaro.require_keys("application_url", "company_name", "company_email", "admin_email", "admin_password")

# Normalize application_url
if ENV['application_url'].end_with? '/'
  ENV['application_url'] = ENV['application_url'][0..-2]
end
