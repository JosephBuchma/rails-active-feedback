ActiveAdmin.register Status do

  permit_params :name

  index do
    selectable_column
    id_column
    column :name
    column :is_initial
    column :created_at
    actions
  end

  filter :name
  filter :created_at

  form do |f|
    f.inputs "New Status" do
      f.input :name
    end
    f.actions
  end

end
