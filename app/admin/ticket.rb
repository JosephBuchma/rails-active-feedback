ActiveAdmin.register Ticket do
  permit_params :department, :status, :subject, :body, :consumer_name, :consumer_email

  index do
    selectable_column
    id_column
    column :subject
    column :status
    column :department
    column :assignee
    column :consumer_name
    actions
  end

  filter :subject
  filter :status
  filter :department
  filter :assignee
  filter :consumer_name

  form do |f|
    f.inputs "Admin Details" do
      f.input :subject
      f.input :body
      f.input :consumer_name
      f.input :consumer_email
      f.input :department
      f.input :assignee
    end
    f.actions
  end

end
