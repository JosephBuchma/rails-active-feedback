ActiveAdmin.register User do
  permit_params :username, :first_name, :last_name, :email

  index do
    selectable_column
    id_column
    column :username
    column :first_name
    column :last_name
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :username
  filter :first_name
  filter :last_name
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "User details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :username
    end
    f.actions
  end

end
