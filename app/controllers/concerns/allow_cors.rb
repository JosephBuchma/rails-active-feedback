module AllowCors
  extend ActiveSupport::Concern

  included do
    before_filter :cors_before_filter
  end


  def cors_before_filter
    if Origin.allowed? request.headers['HTTP_ORIGIN']
      headers['Access-Control-Allow-Origin'] = request.headers['HTTP_ORIGIN']
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
      headers['Access-Control-Allow-Methods'] = 'POST'
    end
  end  
end