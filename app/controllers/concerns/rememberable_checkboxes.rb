module RememberableCheckboxes
  extend ActiveSupport::Concern

  ###
  # TODO: write docs
  # 


  def init_selected(name, methods: ['cont_any'])
    self.instance_variable_set("@selected_#{name}", {})
    if params[:q]
      selected_params = {}
      methods.each do |method|
        if params[:q].keys.collect{ |key| key.end_with?(method)}.any?
          unless selected_params[method]
            selected_params[method] = []
          end
          if params[:q]["#{name.singularize}_#{method}"].kind_of?(Array)
            selected_params[method] += params[:q]["#{name.singularize}_#{method}"]
          else
            selected_params[method] << params[:q]["#{name.singularize}_#{method}"]
          end
        end
      end
      self.instance_variable_set("@selected_#{name}", selected_params)
    end
  end


end
