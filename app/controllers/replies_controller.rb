class RepliesController < ApplicationController
  inherit_resources

  def create
    @reply = Reply.new(reply_params)
    if @reply.save
      @ticket = @reply.ticket
      redirect_to ticket_path @reply.ticket_id
    else
      render json: @reply.errors.to_json
    end
  end

  private

  def reply_params
    params.require(:reply).permit(:author_id, :ticket_id, :reply_body)
  end

end
