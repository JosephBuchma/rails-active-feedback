class TicketsController < ApplicationController

  include RememberableCheckboxes
  include AllowCors

  inherit_resources

  skip_before_filter :verify_authenticity_token, :only => [:create]
  before_filter :authenticate_user!, except: [:create, :show, :edit, :update]



  def index
    @q = Ticket.ransack(params[:q])
    @tickets = @q.result.page(params[:page]).to_a.uniq

    init_selected 'department_names'
    init_selected 'status_names'
    init_selected 'assignee_ids', methods: ['eq', 'null']
  end

  def show
    if current_user
      @ticket = Ticket.where(id: params[:id]).includes(replies: :author).first!
      @reply = Reply.new(author_id: current_user.id, ticket_id: @ticket.id)
    else
      @ticket = Ticket.with_token(params[:id])
    end
  end

  def new
    new! layout: 'embeded_ticket_form'
  end

  def edit
    if current_user
      redirect_to ticket_path(params[:id])
    else
      @ticket = Ticket.with_token(params[:id])
    end
  end

  def update
    if current_user
      update!
    else
      @ticket = Ticket.with_token(params[:id])
      @ticket.update_attributes(ticket_params)
      @ticket.save()
      if @ticket.errors.any?
        render :edit
      else
        render :show, id: params[:id], flash:{notice: 'Updated'}
      end
    end
  end

  def create
    t = Ticket.new(ticket_params)
    if t.save()
      render json: 'thank you!'
    else
      render json: t.errors
    end
  end


  def versions
    ticket = Ticket.find(params[:id])
    @versions = ticket.get_versions_with_consumer_changes
  end


  private

  @@public_params_for_update = [
    :subject, 
    :body, 
    :consumer_name,
    :updated_by_consumer_at
  ]

  @@public_params_for_create = [
    :department_id, 
    :subject, 
    :body, 
    :consumer_name, 
    :consumer_email,
    :updated_by_consumer_at
  ]

  @@staff_permitted = @@public_params_for_create & @@public_params_for_update.push(:assignee_id, :status_id, :updated_by_staff_at)

  def ticket_params
    if current_user
        params.require(:ticket).permit(*@@staff_permitted).merge({updated_by_staff_at: DateTime.now})
    else
      if action_name == 'update'
        par = @@public_params_for_update
      elsif action_name == 'create'
        par = @@public_params_for_create
      end
      params.require(:ticket).permit(*par).merge({updated_by_consumer_at: DateTime.now})
    end
  end
end
