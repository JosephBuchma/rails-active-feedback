class Reply < ActiveRecord::Base

  belongs_to :ticket
  belongs_to :author, class_name: "User"

  validates :author, :ticket, :reply_body,
    presence: true

  after_create :email_consumer


  private

  def email_consumer
    ConsumerMailer.ticket_replied_email(self).deliver_now
  end


end
