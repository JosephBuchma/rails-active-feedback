class User < ActiveRecord::Base
  devise :database_authenticatable,
         :rememberable, :trackable, :validatable,
         :authentication_keys => [ :username ]


  has_many :tickets, foreign_key: :assignee_id
  has_many :replies, foreign_key: :author_id

  validates :first_name, :last_name, :email, :username, :password, 
    presence: true

  validates :email, :username, 
    uniqueness: true

  validates :email, 
    email: true
    

  before_validation :generate_password, on: :create
  after_create :email_new_user


  private


  def generate_password
    self.password ||= SecureRandom.random_number(9999999999).to_s
  end

  def email_new_user
    UserMailer.login_info_email(self).deliver_now
  end

end
