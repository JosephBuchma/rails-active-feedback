class Ticket < ActiveRecord::Base

  include PublicActivity::Model
  include UniqueToken
  has_paper_trail
  tracked owner: Proc.new{ |controller, model| 
    controller.current_user ? controller.current_user : model
  }

  belongs_to :status
  belongs_to :assignee, class_name: "User"
  belongs_to :department
  has_many :replies

  validates :department, :status, :subject, :body, :consumer_name, :consumer_email,
    presence: true

  validates :consumer_email, 
    email: true

  before_validation :set_status, on: :create
  after_create :email_consumer


  scope :assigned_to_user_with_id, -> (user_id) { where assignee_id: user_id }
  scope :unassigned, -> { where assignee_id: nil }
  scope :with_token, -> (token) { where(unique_token: token).first! }
  

  def get_versions_with_consumer_changes
    versions = []
    self.versions.each do |v|
      unless v.reify.nil? or v.changeset.nil?
        unless (v.changeset.keys & ['subject', 'body']).empty?
          versions << v.reify
        end
      end   
    end
    versions
  end

  def self.ransackable_scopes(auth_object = nil)
    [:unassigned, :assigned_to_user_with_id]
  end


  private


  def email_consumer
    ConsumerMailer.ticket_reference_email(self).deliver_now
  end

  def set_status
    self.status = Status.initial
  end

end
