class Origin < ActiveRecord::Base

  def self.allowed?(origin)
    where(origin: origin, active: true).exists?
  end

end
