module UniqueToken
  extend ActiveSupport::Concern

  included do
    before_validation :set_unique_token, on: :create
    attr_readonly :unique_token
  end

  def generate_token
    hex = SecureRandom.hex(2)
    chars = SecureRandom.urlsafe_base64(5)[0..2]
    "#{chars}-#{hex[0..1]}-#{chars}-#{hex[2..3]}-#{chars}".upcase
  end

  def set_unique_token
    token = generate_token()
    while self.class.where(unique_token: token).exists?
      token = generate_token()
    end
    self.unique_token = token
  end
end
