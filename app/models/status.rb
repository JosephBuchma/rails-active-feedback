class Status < ActiveRecord::Base

  has_many :tickets


  scope :initial, -> { where(is_initial: true).first }


  def is_initial?
    self[:is_initial]
  end

end
