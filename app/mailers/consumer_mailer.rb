class ConsumerMailer < ApplicationMailer

  def ticket_reference_email(ticket)
    @ticket = ticket.decorate
    mail(to: ticket.consumer_email, subject: "#{ENV['company_name']} - #{ticket.subject}")
  end

  def ticket_replied_email(reply)
    @ticket = reply.ticket
    @reply = reply
    mail(to: @ticket.consumer_email, subject: "#{ENV['company_name']} - #{@ticket.subject}")
  end
end
