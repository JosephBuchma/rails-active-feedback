class UserMailer < ApplicationMailer

  ###
  # Send email with login information (username, email)
  # to newly created by admin staff member
  # 
  def login_info_email(user)
    @user = user.decorate
    mail(to: user.email, subject: 'Sign In credentinals')
  end
end
