class ApplicationMailer < ActionMailer::Base
  default from: "#{ENV['company_email']}"
  layout 'mailer'
end
