


loadVersions = ->
  $.getJSON "#{window.location.pathname}/versions", (versions)->
    raw_versions = versions
    $ticket = $('#ticket')
    $tickets_container = $('#tickets_container')
    for v in versions.reverse()
      new_ticket = $ticket.clone()
      new_ticket.find('.ticket_subject').html(v.subject)
      new_ticket.find('.ticket_body').html(v.body)
      new_ticket.find('.ticket_created_at').html(v.updated_at)
      new_ticket.addClass('.loaded_version')
      $tickets_container.append(new_ticket)


$ ->
  $('#diffbutton').on 'click', (e)->
    this.disabled = true
    loadVersions()

  $('#no_assignee_filter').on 'change', (e)->
    if $(this).is(':checked')
      $('#current_user_filter').attr('checked', false)

  $('#current_user_filter').on 'change', (e)->
    if $(this).is(':checked')
      $('#no_assignee_filter').attr('checked', false)
  
  $('.search_checkbox').on 'change', (e)->
    $('#search_form').submit()


  typingTimer = undefined
  doneTypingInterval = 300

  doneTyping = ->
    $('#search_form').submit()


  $('#q_subject_or_body_cont').keyup ->
    clearTimeout typingTimer
    typingTimer = setTimeout(doneTyping, doneTypingInterval)
    return

  $('#q_subject_or_body_cont').keydown ->
    clearTimeout typingTimer
    return

  $("#theForm").ajaxForm({url: 'http://0.0.0.0:3000/tickets/', type: 'post'});
