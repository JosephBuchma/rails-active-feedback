module TicketsHelper
  def departments
    Department.all
  end

  def statuses
    Status.all
  end

  def search_input(tag_name, name, operator, value, selected, id: nil)
    arr = operator == 'cont_any' ? '[]' : '' 
    send( 
      tag_name, 
      "q[#{name}_#{operator}]#{arr}", 
      value, 
      !selected[operator].nil? && selected[operator].include?(value), 
      id: id.nil? ? "#{name}_#{operator}_#{value}" : id, 
      class: 'search_checkbox')
  end

end
