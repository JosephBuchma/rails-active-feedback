json.extract! @ticket, :id, :department_id, :status_id, :subject, :body, :consumer_name, :consumer_email, :unique_token, :assignee_id, :created_at, :updated_at
