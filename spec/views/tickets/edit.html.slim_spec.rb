require 'rails_helper'

RSpec.describe "tickets/edit", type: :view do
  before(:each) do
    @ticket = assign(:ticket, Ticket.create!(
      :department_id => 1,
      :status_id => 1,
      :subject => "MyString",
      :body => "MyText",
      :consumer_name => "MyString",
      :consumer_email => "MyString",
      :unique_token => "MyString",
      :assignee_id => "MyString"
    ))
  end

  it "renders the edit ticket form" do
    render

    assert_select "form[action=?][method=?]", ticket_path(@ticket), "post" do

      assert_select "input#ticket_department_id[name=?]", "ticket[department_id]"

      assert_select "input#ticket_status_id[name=?]", "ticket[status_id]"

      assert_select "input#ticket_subject[name=?]", "ticket[subject]"

      assert_select "textarea#ticket_body[name=?]", "ticket[body]"

      assert_select "input#ticket_consumer_name[name=?]", "ticket[consumer_name]"

      assert_select "input#ticket_consumer_email[name=?]", "ticket[consumer_email]"

      assert_select "input#ticket_unique_token[name=?]", "ticket[unique_token]"

      assert_select "input#ticket_assignee_id[name=?]", "ticket[assignee_id]"
    end
  end
end
