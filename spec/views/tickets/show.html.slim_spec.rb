require 'rails_helper'

RSpec.describe "tickets/show", type: :view do
  before(:each) do
    @ticket = assign(:ticket, Ticket.create!(
      :department_id => 1,
      :status_id => 2,
      :subject => "Subject",
      :body => "MyText",
      :consumer_name => "Consumer Name",
      :consumer_email => "Consumer Email",
      :unique_token => "Unique Token",
      :assignee_id => "Assignee"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Subject/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Consumer Name/)
    expect(rendered).to match(/Consumer Email/)
    expect(rendered).to match(/Unique Token/)
    expect(rendered).to match(/Assignee/)
  end
end
