require 'rails_helper'

RSpec.describe "tickets/index", type: :view do
  before(:each) do
    assign(:tickets, [
      Ticket.create!(
        :department_id => 1,
        :status_id => 2,
        :subject => "Subject",
        :body => "MyText",
        :consumer_name => "Consumer Name",
        :consumer_email => "Consumer Email",
        :unique_token => "Unique Token",
        :assignee_id => "Assignee"
      ),
      Ticket.create!(
        :department_id => 1,
        :status_id => 2,
        :subject => "Subject",
        :body => "MyText",
        :consumer_name => "Consumer Name",
        :consumer_email => "Consumer Email",
        :unique_token => "Unique Token",
        :assignee_id => "Assignee"
      )
    ])
  end

  it "renders a list of tickets" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Subject".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Consumer Name".to_s, :count => 2
    assert_select "tr>td", :text => "Consumer Email".to_s, :count => 2
    assert_select "tr>td", :text => "Unique Token".to_s, :count => 2
    assert_select "tr>td", :text => "Assignee".to_s, :count => 2
  end
end
