# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'database_cleaner'
require 'factory_girl_rails'
require 'ffaker'

ActiveRecord::Migration.maintain_test_schema!

# default strategy
DatabaseCleaner.strategy = :transaction
DatabaseCleaner.clean_with :truncation

RSpec.configure do |config|
  # Load DSLs
  config.include FactoryGirl::Syntax::Methods

  # Factory Girl: check factories are valid
  config.before(:suite) do
    begin
      DatabaseCleaner.start
      #FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end

  # Wrap all tests with Database Cleaner
  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end
