## Rails Active Feedback
Feedback management system.

### Main features
   + Quick setup, brand-agnostic :)
   + Dynamic feedback form loading (like disqus)
   + Feedback submit post can be submitted only for allowed hosts, which can be adjusted in admin interface (dynamic `Access-Control-Allow-Origin` header)



### Installation
Before running migrations you shold add `application.yml` file to `config` folder.
`application.yml` required parameters:
   + *company_name*   - name of company will be shown in appropriate places
   + *company_email*  - will be used for email responses
   + *admin_email*    - email for initial admin user
   + *admin_password* - password for initial admin user
   + *statuses* - contains array of ticket statuses. First status in array will be marked as initial (it will be automatically assigned to newly created tickets)
   + *departments* - contains array of departments names
  

Also, after installation allowed hosts (origins) to handle feedback from should be added in the admin interface.
And of course admin can edit departments and statuses in admin interface (Users).
New staff should be registered by admin in the. After registration staff member receives email with password for singing in.

#### Example `aplication.yml`
```
company_name: "Active Feedback"

company_email: "noreply@activefeedback.com"

admin_email: "admin@activefeedback.com"
admin_password: "password"

statuses: "['Waiting for Staff Response', 'Waiting for Customer', 'On Hold, Cancelled', 'Completed']"
departments: "['General support', 'Sales', 'Security']"

```


### TODO
   + Handle managing of initial status
   + Cache statuses and departments using redis or memcached
   + Add ability to change password for staff
   + Pagination
   + Create script for loading feedback form asynchronously like disqus
   + Rake task for re-rendering feedback_form.js (the script for loading form asynchronously, like disqus), and run it when departments edited, so departments list will be always up to date in feedback form
   + asynchronousy loaded form should handle validation on cliend side
   + separate customers and staff actions into different controllers
   + Make feeds timeline more informative
   + add allowed_hosts option to application.yml
   + send all emails asynchronously (actually, it was done before I decided to deploy it on heroku, where additional worker for queue is not free, so I returned deliver_now back)
   + replace all seeds from migrations to seeds :)
   + **write tests**

# WHAT IS TOTALLY BAD, AND HOW I THINK IT SHOULD BE DONE
   + tickets controller. It should be separated to 2 controllers, for staff and for costumers respectively, and depending on routes appropriate controller should be chosen. Everything should be DRY, clear and maintainable. There is no reason to divide ticket show view I guess.
   + What bad in logic - replies and 'edits' from user should be done like dialog between staff and customer.
   + Shitty commits (I mean commit messages, and amount of code that is commited, and actually what is commited. In real life I don't committing in this manner:) )