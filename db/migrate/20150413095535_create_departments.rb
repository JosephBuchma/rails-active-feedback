class CreateDepartments < ActiveRecord::Migration
  def migrate(direction)
    super
    if direction == :up
      if ENV['departments']
        YAML.load(ENV['departments']).each do |department|
          Department.create!(name: department)
        end
      end
    end
  end

  def change
    create_table :departments do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
