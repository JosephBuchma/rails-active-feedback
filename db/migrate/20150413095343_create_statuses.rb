class CreateStatuses < ActiveRecord::Migration
  def migrate(direction)
    super
    if direction == :up
      if ENV['statuses']
        YAML.load(ENV['statuses']).each_with_index do |status, i|
          Status.create!(name: status, is_initial: i==0)
        end
      else
        Status.create!(name: "Waiting for staff response", is_initial: i==0)
      end
    end
  end

  def change
    create_table :statuses do |t|
      t.string :name
      t.boolean :is_initial, default: false
      t.timestamps null: false
    end
  end
end
