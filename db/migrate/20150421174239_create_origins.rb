class CreateOrigins < ActiveRecord::Migration
  def change
    create_table :origins do |t|
      t.string :origin
      t.boolean :active

      t.timestamps null: false
    end
  end
end
