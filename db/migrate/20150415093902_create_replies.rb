class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.integer :ticket_id, null: false
      t.integer :author_id, null: false
      t.text :reply_body, null: false
      t.timestamps null: false
    end
  end
end
