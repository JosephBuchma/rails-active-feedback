class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :department_id
      t.integer :status_id
      t.string :subject
      t.text :body
      t.string :consumer_name
      t.string :consumer_email
      t.string :unique_token, null: false
      t.integer :assignee_id
      t.datetime :updated_by_consumer_at
      t.datetime :updated_by_staff_at

      t.timestamps null: false
    end
    add_index :tickets, :unique_token, unique: true
  end
end
